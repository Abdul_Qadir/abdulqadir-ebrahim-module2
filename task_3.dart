void main() {
  var mtnWinner = mtnApp();
  mtnWinner.name = 'Ambani Africa';
  mtnWinner.sector = 'Education';
  mtnWinner.developer = 'Mukundi Lambani ';
  mtnWinner.winningYear = 2021;

  mtnWinner.printmtnAppInformation();
}

class mtnApp {
  String? name;
  String? sector;
  String? developer;
  int? winningYear;

  void printmtnAppInformation() {
    name = name?.toUpperCase();
    print('The Apps name is $name .');
    print('The Apps sector is $sector .');
    print('The developer name is $developer .');
    print('The Year the  won is $winningYear .');
  }
}
